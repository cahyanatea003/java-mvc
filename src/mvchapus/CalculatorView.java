/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvchapus;

import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author asep
 */
public class CalculatorView extends JFrame {
    
    private final JTextField firstNumber = new JTextField(10);
    private final JLabel additionLabel = new JLabel("+");
    private final JTextField secondNumber = new JTextField(10);
    private final JButton calculateButton = new JButton("Calculate");
    private final JTextField calcSolution = new JTextField(10);
    
    CalculatorView() {
        JPanel calcPanel = new JPanel();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 200);
        
        calcPanel.add(firstNumber);
        calcPanel.add(additionLabel);
        calcPanel.add(secondNumber);
        calcPanel.add(calculateButton);
        calcPanel.add(calcSolution);
        
        add(calcPanel);
    }
    
    public int getFirstNumber() {
        return Integer.parseInt(firstNumber.getText());
    }
    
    public int getSecondNumber() {
        return Integer.parseInt(secondNumber.getText());
    }
    
    public int getCalcSolution() {
        return Integer.parseInt(calcSolution.getText());
    }
    
    public void setCalcSolution(int solution) {
        calcSolution.setText(Integer.toString(solution));
    }
    
    void addCalculationListener(ActionListener actionListener) {
        calculateButton.addActionListener(actionListener);
    }
    
    void displayErrorMessage(String errorMessage) {
        JOptionPane.showMessageDialog(this, errorMessage);
    }
}
